# README #

A React application built following the
[Front-End Web Development with React](https://www.coursera.org/learn/front-end-react)
course, second of the four courses of the
[Full-Stack Web Development with React Specialization](https://www.coursera.org/specializations/full-stack-react)
offered by Coursera.